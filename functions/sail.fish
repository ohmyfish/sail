function sail --wraps ./vendor/bin/sail --description 'Alias for Laravel Sail command'
    ./vendor/bin/sail $argv
end
