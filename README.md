A simple alias to Laravel Sail.

# <img src="https://cdn.rawgit.com/oh-my-fish/oh-my-fish/e4f1c2e0219a17e2c748b824004c8d0b38055c16/docs/logo.svg" width="28px" height="28px"/> sail

A plugin for [Oh My Fish][oh-my-fish]. 

[![MIT License](https://img.shields.io/badge/License-MIT-blue?style=for-the-badge)](LICENSE.md)
[![Fish Shell](https://img.shields.io/badge/fish-3.1.0-blue?style=for-the-badge)](https://fishshell.com)
[![Oh My Fish](https://img.shields.io/badge/Oh%20My%20Fish-Fishshell--Framework-blue?style=for-the-badge)](https://github.com/oh-my-fish/oh-my-fish)

**sail** just avoids the need to type _vendor/bin/_ before `sail`.

## Install

```shell script
$ omf install sail
```

## Usage

Use the [standard sail commands](https://laravel.com/docs/8.x/sail), just without `./vendor/bin/`.

# License

[MIT][mit] © [Marc-André Appel][author]

[oh-my-fish]: https://www.github.com/oh-my-fish/oh-my-fish
[original]: https://github.com/rubiev
[author]: https://gitlab.com/marc-andre
[mit]: /LICENSE.md
